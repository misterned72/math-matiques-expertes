# -*- coding:UTF-8 -*-

import rsa_utils
import arith
import random

# 1.4 Détails pratiques

pythagore = "In omni triangulo rectangulo, quadratum lateris quod \
recto angulo opponitur, aequale est duobus simul reliquorum laterum \
quadratis."

print('----------------------------------------------------')
print('Message : %s' % pythagore)
print('----------------------------------------------------')

# 1.5 Convertir une chaîne en nombres

def chaine_vers_nombre(s):
  return rsa_utils.string_to_number(s)

def chaine_vers_chiffres(s, b):
  return rsa_utils.string_to_digits(s, b)

x = chaine_vers_chiffres(pythagore, 1234567)
print('Le message converti en chiffres en base 1234567 : %s' % x)
print('----------------------------------------------------')

# 1.6 Convertir des nombres en chaîne

def chiffres_vers_chaine(x, b):
  return rsa_utils.digits_to_string(x, b)


print('Mauvais choix de base : %s' % (chiffres_vers_chaine(x, 7654321)))
print('----------------------------------------------------')
print('Bon choix de base : %s' % (chiffres_vers_chaine(x, 1234567)))
print('----------------------------------------------------')

# 2.1 Bob crée ses clés RSA

p = 137
q = 151

e = 142117

def inverse_mod(a, b):
  u, v, d = arith.ext_gcd(a, b)
  return u % b

d = inverse_mod(e, (p - 1) * (q - 1))

print ('Inverse de 142117 modulo 136*150 : %d' % d)
print('----------------------------------------------------')

# 2.2 Automatisation du procédé

def creerCle(L):
  e = 65537
  n1 = 2 ** (L // 2 - 1)
  n2 = 2 ** (L // 2) - 1
  p = random.randint(n1, n2)
  while not(arith.is_prime(p)) or arith.gcd(p - 1, e) != 1: p = p + 1
  q = random.randint(n1, n2)
  while not(arith.is_prime(q)) or arith.gcd(q - 1, e) != 1: q = q + 1
  n = p * q
  nn = (p - 1) * (q - 1)
  d = inverse_mod(e, nn)
  return (n, e, d)

nB, eB, dB = creerCle(64)
print('Une clé de 64 bits : %d, %d, %d' % (nB, eB, dB))
print('----------------------------------------------------')

def casser_RSA(n, e):
  p = arith.rho_pollard(n)
  q = n // p
  n1 = (p - 1) * (q - 1)
  d = inverse_mod(e, n1)
  return (n, e, d)


u, v, w = casser_RSA(nB, eB)
print('Clé trop faible, peut être cassée : %d, %d, %d' % (u, v, w))
print('----------------------------------------------------')

# 2.3 Crypter, décrypter

def rsa(s, e, n):
  xs = chaine_vers_chiffres(s, n)
  ys = [arith.power_mod(x, e, n) for x in xs]
  return chiffres_vers_chaine(ys, n)

grotehapy = rsa(pythagore, eB, nB)

print('Le message crypté : %s' % grotehapy)
print('----------------------------------------------------')
print('Le message décrypté = %s' % (rsa(grotehapy, dB, nB)))
print('----------------------------------------------------')

# 4.1 Alice crée sa clé RSA

nA, eA, dA = creerCle(64)

# 4.2 Alice envoie un message "signé numériquement"

toryphage = rsa(rsa(pythagore, dA, nA), eB, nB)

print('Message crypté et signé : %s' % toryphage)
print('----------------------------------------------------')

# 4.3 Bob décrypte le message et vérifie sa provenance 

print('Message décrypté et validé : % s' % (rsa(rsa(toryphage, dB, nB), eA, nA)))
print('----------------------------------------------------')


