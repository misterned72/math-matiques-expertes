# -*- coding: utf-8 -*-
from nbautoeval.exercise_function import ExerciseFunction
from nbautoeval.args import Args


# @BEG@ name=eratosthene
def eratosthene(n):
    """retourne la liste des nombres premiers <= n (crible d'eratosthene)"""
    n += 1
    tableau = [0,0] + [i for i in range(2, n)]
    for i in range(2, n):
        if tableau[i] != 0:
            # c'est un nombre 1er: on garde, mais on neutralise ses multiples
            for j in range(i*2, n, i):
                tableau[j] = 0
    return [p for p in tableau if p!=0]


# @END@




inputs_eratosthene = [
    Args(10),
    Args(20),
    Args(50),
    Args(80),
    Args(100),
    Args(120),
    Args(150),
    Args(200),
    Args(300),
]


exo_eratosthene = ExerciseFunction(
    eratosthene, inputs_eratosthene,
    nb_examples = 6,
)
