def eratosthene(n):
    """retourne la liste des nombres premiers <= n (crible d'eratosthene)"""
    n += 1
    tableau = [0,0] + [i for i in range(2, n)]
    for i in range(2, n):
        if tableau[i] != 0:
            # c'est un nombre 1er: on garde, mais on neutralise ses multiples
            for j in range(i*2, n, i):
                tableau[j] = 0
    return [p for p in tableau if p!=0]

'''Le code se lit bien:

on crée une liste de nombres comme [0,0,2,3,4,5,6,7,8,9,10,…]
le nombre d'indice 1 est 0, parce que 1 n'est pas un nombre premier
on progresse dans la liste, et à chaque fois qu'on trouve un nombre différent de 0: c'est un nombre premier, et on met à 0 tous ses multiples
à la fin, on retourne le liste de tous les nombres qui ne sont pas à 0'''

'''C'est déjà un code rapide, mais on peut l'améliorer:

une fois le nombre 2 identifié comme premier, on ne testera plus que les nombres impairs
dans la boucle de test, une fois qu'on a atteint un nombre premier supérieur à racine de n, il n'y a plus de multiple à supprimer
dans le tableau, on va plutôt contenir des booléens (True/False) que des nombres
Voilà un tel code amélioré:'''

def eratosthene_due(n):
    """retourne la liste des nombres premiers <= n (crible d'eratosthene)"""
    if n<2:
        return []
    n += 1
    tableau = [False,False] + [True]*(n-2)
    tableau[2::2] = [False]*((n-2)//2 + n%2) # sup. des nb pairs
    premiers = [2] # initialisation de la tableau des nb 1ers (2 est 1er)
    racine = int(n**0.5)
    racine = racine + [1,0][racine%2] # pour que racine soit impair
    for i in range(3, racine+1, 2):
        if tableau[i]:
            # on enregistre le nouveau nb 1er
            premiers.append(i)
            # on élimine i et ses multiples
            tableau[i::i] = [False]*((n-i)//i + int((n-i)%i>0)) 
    for i in range(racine, n, 2):
        if tableau[i]:
            # on enregistre le nouveau nb 1er (pas de multiples à supprimer)
            premiers.append(i)
    return premiers

'''Cette version est de plus très rapide. Par exemple, on trouve les 664579 nombres premiers inférieurs à 10 millions en une seule seconde!!! Et les 5761455 nombres premiers inférieurs à 100 millions en une dizaine de secondes.

Mais au delà, malheureusement, cette méthode consomme beaucoup de mémoire et génère une exception “MemoryError”.

Pour contourner ce problème de mémoire, commençons par créer un itérateur pour éviter d'avoir à contenir en mémoire toute la liste des nombres premiers:'''

def eratosthene_iter(n):
    """Itérateur retourne tous les nb premiers <= n (crible d'Eratosthene)"""
    if n<2:
        pass # il n'y a aucun nb 1er en dessous de 2!
    else:
        n += 1 # pour avoir les nb 1ers <=n et pas seulement <n
        tableau = [False, False] + [True]*(n-2)
        tableau[2::2] = [False]*((n-2)//2 + n%2) # supr. des nb pairs
        yield 2 # 2 est un nombre premier
        racine = int(n**0.5)
        racine = racine + [1,0][racine%2] # pour que racine soit impair
        i, fin, pas = 3, racine+1, 2
        while i<fin: # on ne traite que les nb impairs
            if tableau[i]:
                yield i # i est un nombre premier
                # on élimine i et ses multiples
                tableau[i::i] = [False]*((n-i)//i + int((n-i)%i>0)) 
            i += pas
        i, fin, pas = racine, n, 2
        while i<fin:  # on ne traite que les nb impairs
            if tableau[i]:
                yield i # i est un nombre premier
            i += pas

'''Comme c'est un itérateur, on peut toujours retrouver la liste complète des nombres renvoyés:

premiers = [p for p in eratosthene_iter(1000)]
On peut même créer une fonction lambda:

premiers = lambda n: [p for p in eratosthene_iter(n)]'''


