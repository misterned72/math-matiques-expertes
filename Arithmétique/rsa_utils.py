# -*- coding:UTF-8 -*-

nb_lettres = 256

# Convertir une liste de chiffres en base b en un nombre
def digits_to_number(xs, b):
  ys = xs[:]
  n = 0
  pw = 1
  ys.reverse()
  for x in ys:
     n += x * pw
     pw = pw * b
  return n

# Convertir un nombre en une liste de chiffres en base b
def number_to_digits(x, b):
  s = []
  while x != 0:
    s.append(x % b)
    x = x // b
  s.reverse()
  return s

# Convertir une chaîne de caractères en nombre
def string_to_number(s):
  return digits_to_number([ord(x) for x in s], nb_lettres)

# Convertir un nombre en chaîne de caractères
def number_to_string(n):
  s = ''
  while n != 0:
    s = chr(n % nb_lettres) + s
    n = n // nb_lettres
  return s

# Convertir une chaîne de caractères en liste de chiffres en base b
def string_to_digits(s, b):
  return number_to_digits(string_to_number(s), b)

# Convertir une suite de chiffres en base b en chaîne de caractères
def digits_to_string(xs, b):
  return number_to_string(digits_to_number(xs, b))


