# -*- coding: UTF-8 -*-

#------------------------------------------------------------------
# Test de Primalité de Miller-Rabin

# Exponentiation rapide modulaire. Renvoie x^n mod p
def power_mod(x, n, p):
  z = 1
  m = n
  y = x
  while m != 0:
    if m % 2 == 1: z = (z * y) % p
    m = m // 2
    y = (y * y) % p
  return z

# Renvoie (s, d) où n - 1 = 2^s d et d est impair
def factor2(n):
  d = n - 1
  s = 0
  while d % 2 == 0:
    s += 1
    d = d // 2
  return (s, d)

# Témoin de non-primalité
# Renvoie True si a est un témoin de non primalité pour n
# Renvoie False sinon
# En clair :
#   - si witness(a, n) == True, n n'est pas premier
#   - si witness(a, n) == False, n est premier avec une probabilité > 3/4

def witness(a, n):
  s, d = factor2(n)
  x = power_mod(a, d, n)
  if x == 1 or x == n - 1: return False
  for r in range(1, s):
    x = (x * x) % n
    if x == 1: return True
    if x == n - 1: return False
  return True



# Test de primalité (probabiliste) de Miller-Rabin
def is_prime(n):
  if n <= 10000: return is_naive_prime(n)
  if n % 2 == 0: return False
  for a in [2, 3, 5, 7, 11,13, 17, 19, 23]:
    if witness(a, n): return False
  return True

#--------------------------------------------------------------------
# Test naïf de primalité

def is_naive_prime(n):
  if n <= 1: return False
  if n == 2: return True
  return least_divisor(n) == n

def least_divisor(n):
  k = 2
  while k * k <= n and n % k != 0: k += 1
  if k * k > n: return n
  else: return k

#--------------------------------------------------------------------
# Recherche d'un facteur non trivial de n - Méthode de Pollard
# L'algorithme ci-dessous échoue lorsque n est un nombre premier.

def rho_pollard(n):
  def f(x): return (x * x + 1) % n
  x = 2
  y = 2
  d = 1
  while True:
    while d == 1:
      x = f(x)
      y = f(f(y))
      d = gcd(abs(x-y), n)
    if d != n: return d

#--------------------------------------------------------------------
# Plus petit nombre premier strictement supérieur à n
def next_prime(n):
  p = n + 1
  while not(is_prime(p)): p += 1
  return p
  
# pgcd
def gcd(a, b):
  while b != 0:
    a, b = b, a % b
  return a

# pgcd étendu. Renvoie un triplet (u, v, d) tel que ua + vb = d  
def ext_gcd(a, b):
  u1, v1, r1 = 1, 0, a
  u2, v2, r2 = 0, 1, b
  while r2 != 0:
    q = r1 // r2
    u, v, r = u1 - q * u2, v1 - q * v2, r1 - q * r2
    u1, v1, r1 = u2, v2, r2
    u2, v2, r2 = u, v, r
  return (u1, v1, r1) 
  