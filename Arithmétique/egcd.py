def egcd(a, b):
    # Algorithme d'Euclide étendu
    s1 = 1
    s2 = 0
    t1 = 0
    t2 = 1
    if a < b:
        r1 = b
        r2 = a
    else:
        r1 = a
        r2 = b
    while True:
        quotient = r1 // r2
        reste = r1 % r2
        # stoppe quand le reste est nul.
        # sinon on continue le processus.
        if reste != 0:
            s = s1 - s2 * quotient
            t = t1 - t2 * quotient
            r1 = r2
            r2 = reste
            s1 = s2
            s2 = s
            t1 = t2
            t2 = t
        else:
            gcd = r2
            break
    return gcd, s2, t2

def bezout(a, b):
    (u0, v0, u1, v1)=(1, 0, 0, 1)
    while b:
        (q, r) = divmod(a, b)
        (a, b) = (b, r)
        (u0, v0, u1, v1) = (u1, v1, u0-q*u1, v0-q*v1)
    return (u0, v0)


def bezout_recursif(a, b):
    if b == 0:
        return (1, 0)
    else:
        (u, v) = bezout(b, a % b)
    return (v, u - (a // b) * v)

print(bezout(12,42))
